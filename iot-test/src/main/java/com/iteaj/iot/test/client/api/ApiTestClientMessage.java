package com.iteaj.iot.test.client.api;

import com.iteaj.iot.client.ClientMessage;
import com.iteaj.iot.message.DefaultMessageHead;

public class ApiTestClientMessage extends ClientMessage {

    public ApiTestClientMessage(byte[] message) {
        super(message);
    }

    public ApiTestClientMessage(MessageHead head) {
        super(head);
    }

    @Override
    protected MessageHead doBuild(byte[] message) {
        return new DefaultMessageHead(null, getChannelId(), null, message);
    }
}
