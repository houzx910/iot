package com.iteaj.iot.test.mqtt;

import com.iteaj.iot.client.ClientProtocolHandle;
import com.iteaj.iot.client.mqtt.gateway.MqttGatewayConnectProperties;
import com.iteaj.iot.client.mqtt.gateway.adapter.MqttGatewayJsonHandle;
import com.iteaj.iot.test.IotTestProperties;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;

@Component
public class MqttGatewayTestHandle implements ClientProtocolHandle<MqttSubscribeTestProtocol>
        , MqttGatewayJsonHandle<MqttSubscribeTestProtocol, Object>, InitializingBean {

    @Autowired
    private IotTestProperties iotTestProperties;
    private MqttGatewayConnectProperties properties;

    @Override
    public Object handle(MqttSubscribeTestProtocol protocol) {
        return new MqttGatewayEntity();
    }

    @Override
    public MqttGatewayConnectProperties getProperties(Object entity) {
        return this.properties;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        IotTestProperties.TestMqttConnectProperties mqtt = iotTestProperties.getMqtt();
        if(mqtt != null) {
            this.properties = new MqttGatewayConnectProperties(mqtt.getHost(), mqtt.getPort(), "MqttGatewayTestClientId", "/mqtt/gateway");
        }
    }
}
