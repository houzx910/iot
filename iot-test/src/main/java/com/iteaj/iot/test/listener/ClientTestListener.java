package com.iteaj.iot.test.listener;

import com.iteaj.iot.client.ClientComponent;
import com.iteaj.iot.client.ClientEventListener;
import com.iteaj.iot.client.IotClient;
import com.iteaj.iot.client.SocketClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 客户端上下线同步事件监听
 */
public class ClientTestListener implements ClientEventListener {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void online(IotClient source, ClientComponent component) {
        SocketClient client = (SocketClient) source;
        logger.info("客户端同步事件监听({}) 上线 - 客户端：{} - 测试通过", component.getName(), client.getConfig().connectKey());
    }

    @Override
    public void offline(IotClient source, ClientComponent component) {
        SocketClient client = (SocketClient) source;
        logger.info("客户端同步事件监听({}) 掉线 - 客户端：{} - 测试通过", component.getName(), client.getConfig().connectKey());
    }
}
