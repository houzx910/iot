package com.iteaj.iot.boot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerLifeCyclePostProcessor implements LifeCyclePostProcessor{

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void postProcessBeforeStart() {
        logger.info("执行组件生命周期[start]前钩子函数[LifeCyclePostProcessor]");
    }

    @Override
    public void postProcessAfterStart() {
        logger.info("执行组件生命周期[start]后钩子函数[LifeCyclePostProcessor]");
    }
}
