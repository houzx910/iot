package com.iteaj.iot.format;

/**
 * @see DataFormat#DCBA
 */
public class DCBAFormat extends DataFormatConvert {

    public static DCBAFormat INSTANCE = new DCBAFormat();

    protected DCBAFormat() { }

    @Override
    protected byte[] byte2Transform(byte[] data, int offset) {
        byte[] buffer = new byte[2];
        buffer[0] = data[offset + 0];
        buffer[1] = data[offset + 1];
        return buffer;
    }

    @Override
    protected byte[] byte4Transform(byte[] data, int offset) {
        byte[] buffer = new byte[4];
        buffer[0] = data[offset + 3];
        buffer[1] = data[offset + 2];
        buffer[2] = data[offset + 1];
        buffer[3] = data[offset + 0];
        return buffer;
    }

    @Override
    protected byte[] byte8Transform(byte[] data, int offset) {
        byte[] buffer = new byte[8];
        buffer[0] = data[offset + 7];
        buffer[1] = data[offset + 6];
        buffer[2] = data[offset + 5];
        buffer[3] = data[offset + 4];
        buffer[4] = data[offset + 3];
        buffer[5] = data[offset + 2];
        buffer[6] = data[offset + 1];
        buffer[7] = data[offset + 0];
        return buffer;
    }
}
