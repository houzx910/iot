package com.iteaj.iot.event;

import com.iteaj.iot.FrameworkComponent;

/**
 * 客户端状态事件
 */
public class StatusEvent extends IotEvent {

    /**
     * 在线状态
     */
    private ClientStatus status;

    /**
     * 离线原因
     */
    private OfflineReason reason;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param source the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     */
    public StatusEvent(Object source, ClientStatus status) {
        super(source);
        this.status = status;
        this.reason = OfflineReason.close;
    }

    public StatusEvent(Object source, ClientStatus status, FrameworkComponent component) {
        this(source, status);
        this.setComponent(component);
    }

    public ClientStatus getStatus() {
        return status;
    }

    public void setStatus(ClientStatus status) {
        this.status = status;
    }

    public OfflineReason getReason() {
        return reason;
    }

    public void setReason(OfflineReason reason) {
        this.reason = reason;
    }
}
