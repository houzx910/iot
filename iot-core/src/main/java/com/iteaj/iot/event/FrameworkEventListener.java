package com.iteaj.iot.event;

import java.util.EventListener;

public interface FrameworkEventListener<E extends IotEvent> extends EventListener {

    void onEvent(E event);

    /**
     * 此事件是否匹配监听器
     * @param event
     * @return
     */
    default boolean isMatcher(IotEvent event) {
        return true;
    }
}
