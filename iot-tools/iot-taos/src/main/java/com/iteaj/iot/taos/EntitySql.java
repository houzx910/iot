package com.iteaj.iot.taos;

import com.iteaj.iot.tools.db.ParamValue;

import java.util.List;

public class EntitySql {

    private Object entity;

    private String tableName;

    private List<ParamValue> values;

    public EntitySql(String tableName, Object entity, List<ParamValue> values) {
        this.values = values;
        this.entity = entity;
        this.tableName = tableName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<ParamValue> getValues() {
        return values;
    }

    public void setValues(List<ParamValue> values) {
        this.values = values;
    }

    public Object getEntity() {
        return entity;
    }
}
