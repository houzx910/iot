package com.iteaj.iot.server.protocol;

import com.iteaj.iot.*;
import com.iteaj.iot.business.ProtocolHandleFactory;
import com.iteaj.iot.client.UnWritableProtocolException;
import com.iteaj.iot.consts.ExecStatus;
import com.iteaj.iot.server.ServerMessage;
import com.iteaj.iot.server.ServerSocketProtocol;
import com.iteaj.iot.server.SocketServerComponent;
import com.iteaj.iot.server.TcpServerComponent;
import com.iteaj.iot.server.udp.UdpDeviceManager;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 用来声明此协议是由服务端主动向客户端发起的
 * @param <M>
 */
public abstract class ServerInitiativeProtocol<M extends ServerMessage> extends
        ServerSocketProtocol<M> implements ProtocolPreservable {

    // 默认超时时间10秒
    private long timeout = 10 * 1000;

    private CountDownLatch downLatch; //同步锁
    private FreeProtocolHandle freeProtocolHandle;

    @Override
    public AbstractProtocol exec(ProtocolHandleFactory factory) {

        /**
         * @see #request()
         * @see #isSyncRequest() 如果是同步请求交由调用线程执行 如果是异步调用则交由netty工作线程处理
         */
        if(!isSyncRequest()) {
            return this.exec(getProtocolHandle());
        } else {
            // 对于同步请求没有在主线程释放的问题 https://gitee.com/iteaj/iot/issues/I517T3
            this.releaseLock(); // 释放锁
        }

        return null;
    }

    /**
     * 构建请求报文
     * @return
     */
    @Override
    public ServerInitiativeProtocol buildRequestMessage() {
        try {
            this.requestMessage = this.doBuildRequestMessage();
        } catch (IOException e) {
            throw new ProtocolException("构建请求报文异常", e.getCause());
        }
        return this;
    }

    /**
     * 构建服务端要请求客户端的报文
     * @return
     */
    protected abstract M doBuildRequestMessage() throws IOException;

    /**
     * 对于平台主动向外请求的协议 由于构建响应报文必须等到响应报文的到来<br>
     *     所以此方法不能使用,请使用以下方法
     * @return
     */
    @Override
    public ServerInitiativeProtocol buildResponseMessage() {
        if(logger.isDebugEnabled() && this.responseMessage() != null) {
            Message.MessageHead head = this.responseMessage().getHead();
            String messageId = head != null ? head.getMessageId() : null;
            logger.debug("服务端主动协议 客户端响应平台 - 客户端编号: {} - 协议类型: {} - messageId: {} - 报文: {}"
                    , getEquipCode(), protocolType().getType(), messageId, this.responseMessage());
        }

        doBuildResponseMessage(responseMessage()); return this;
    }

    /**
     * 解析客户端响应给服务端的报文
     * @param message
     */
    protected abstract void doBuildResponseMessage(M message);

    /**
     * 用来做为将请求报文和响应报文进行关联的key
     * 默认用报文头的{@link com.iteaj.iot.Message.MessageHead#getMessageId()}作为key
     * @return
     */
    @Override
    public Object relationKey(){
        return requestMessage().getHead().getMessageId();
    }

    /**
     * 平台主动向外发起请求
     * @see #request(FreeProtocolHandle)
     */
    public void request(ServerProtocolCallHandle handle) throws ProtocolException {
        this.request((FreeProtocolHandle) handle);
    }

    /**
     * 平台主动向外发起请求
     */
    public <P extends ServerInitiativeProtocol<M>> void request(FreeProtocolHandle<P> handle) throws ProtocolException {
        this.freeProtocolHandle = handle;
        request();
    }

    /**
     * 平台主动向外发起请求
     */
    public void request() throws ProtocolException {
        try {
            this.buildRequestMessage();

            //构建完请求协议必须验证请求数据是否存在
            if(null == requestMessage()) {
                throw new IllegalStateException("不存在请求报文");
            }

            //获取客户端管理器
            SocketServerComponent component = FrameworkManager.getComponent(requestMessage().getClass());
            if(component == null) {
                throw new IllegalStateException("获取不到["+responseMessage().getClass().getSimpleName()+"]对应的组件");
            }

            Channel channel;
            if(component instanceof TcpServerComponent) {
                // 获取并校验设备连接
                channel = (Channel) component.getDeviceManager().find(getEquipCode());
                if(channel instanceof Channel) {
                    if(!channel.isActive()) {
                        this.setExecStatus(ExecStatus.offline);
                    }
                } else {
                    // https://gitee.com/iteaj/iot/issues/I7V94U
                    this.setExecStatus(ExecStatus.offline);
                }

                if(this.getExecStatus() != ExecStatus.success) {
                    // 设备不在线, 直接执行业务
                    exec(getProtocolHandle()); return;
                }
            } else {
                channel = ((UdpDeviceManager) component.getDeviceManager()).getChannel();
            }

            this.requestMessageHandle(component, channel);

            if (writeAndFlush(component) != ExecStatus.success) {
                // 写失败, 直接执行业务
                exec(getProtocolHandle());
                return;
            }

            /**
             * 同步请求阻塞线程等待
             * 此处的同步是指让调用线程阻塞, 等待设备响应回调之后解锁{@link #releaseLock()}
             * @see #getTimeout() 等待超时
             */
            if(isSyncRequest()) {
                syncDeadValidate(channel);

                boolean await = getDownLatch().await(getTimeout(), TimeUnit.MILLISECONDS);
                if(!await) {
                    this.execTimeoutHandle(component);
                }

                // 执行业务
                exec(getProtocolHandle());
            }

        } catch (InterruptedException e) {
            throw new ProtocolException("执行中断", e);
        } catch (Exception e) {
            if(e instanceof ProtocolException) {
                throw e;
            } else {
                throw new ProtocolException(e.getMessage(), e);
            }
        }
    }

    /**
     * 做一些请求报文对象{@link ServerMessage}的初始化工作
     * @param component
     * @param channel
     */
    protected void requestMessageHandle(SocketServerComponent component, Channel channel) {
        requestMessage().setChannelId(channel.id().asShortText());
    }

    /**
     * 写报文到客户端
     * @param component
     * @return true 成功  false 失败
     * @throws InterruptedException
     */
    protected ExecStatus writeAndFlush(SocketServerComponent component) throws InterruptedException {
        ExecStatus result = getExecStatus();
        ChannelFuture future = (ChannelFuture) component
                .writeAndFlush(getEquipCode(), this).orElse(null);

        if(future == null) {
            this.setReason("设备不在线");
            setExecStatus(ExecStatus.offline);
            return getExecStatus();
        }

        if(future.isDone()) {
            Throwable cause = future.cause();
            if(cause instanceof NotDeviceException) {
                this.setReason("设备不在线");
                setExecStatus(ExecStatus.offline);
                return getExecStatus();
            }
        } else {
            if(!future.await(getTimeout(), TimeUnit.MILLISECONDS)) {
                this.setReason("请求超时");
                setExecStatus(ExecStatus.timeout);
                return getExecStatus();
            }

            if(!future.isSuccess()) {
                if(future.cause() instanceof UnWritableProtocolException) {
                    this.setExecStatus(result = ExecStatus.notWritable);
                } else {
                    this.setExecStatus(result = ExecStatus.fail);
                }

                if(logger.isDebugEnabled()) {
                    logger.error("发送失败", future.cause());
                }

                this.setReason(future.cause() != null ? future.cause().getMessage() : this.getExecStatus().desc);
            }
        }

        return result;
    }

    /**
     * 同步时的死锁校验
     * 对于同步请求, 当前同步线程和{@code Channel}的工作线程不能是同一个
     * @param channel
     */
    protected void syncDeadValidate(Channel channel) {
        if(channel.eventLoop().inEventLoop()) {
            throw new IllegalThreadStateException("调用线程和工作线程相同将导致死锁");
        }
    }

    protected void execTimeoutHandle(SocketServerComponent component) {
        this.setReason("请求超时");
        setExecStatus(ExecStatus.timeout);
        if(isRelation()) { // 移除掉对应的协议
            try {
                syncRemoveTimeoutProtocol(component);
            } finally {
                // 释放锁
                releaseLock();
            }
        }
    }

    /**
     * 这里的移除将比超时管理器早
     */
    protected void syncRemoveTimeoutProtocol(SocketServerComponent component) {
        Message.MessageHead head = requestMessage().getHead();

        Object protocol = component.protocolFactory().get(head.getMessageId());

        if(protocol != null && protocol == this) {
            component.protocolFactory().remove(head.getMessageId());
            if(logger.isWarnEnabled()) {
                logger.warn("同步超时({}) 超时移除({}ms) - 客户端编号: {} - messageId: {} - 协议类型: {}"
                        , component.getName(), getTimeout(), head.getEquipCode(), head.getMessageId(), protocolType());
            }
        }
    }

    /**
     * 同步调用, 调用的线程将阻塞, 直到超时或者客户端响应
     * @see #releaseLock()
     * @param timeout 阻塞超时时间
     * @return
     */
    public <P extends ServerInitiativeProtocol> P sync(long timeout) {
        setDownLatch(new CountDownLatch(1));
        return (P) timeout(timeout);
    }

    @Override
    public ServerInitiativeProtocol exec(ProtocolHandle handle) {
        // 构建响应报文
        // 不在主线程调用问题 https://gitee.com/iteaj/iot/issues/I517T3
        // 不在主线程捕获问题 https://gitee.com/iteaj/iot/issues/I517Y0
        this.buildResponseMessage();

        if(handle != null) {
            handle.handle(this);
        }

        return null;
    }

    @Override
    public String getEquipCode() {
        return super.getEquipCode();
    }

    protected ProtocolHandle getProtocolHandle() {
        ProtocolHandle protocolHandle = getFreeProtocolHandle();
        if(protocolHandle == null) {
            protocolHandle = FrameworkManager.getProtocolHandle(getClass());
        }

        return protocolHandle;
    }

    @Override
    public long getTimeout() {
        return timeout;
    }

    public FreeProtocolHandle getFreeProtocolHandle() {
        return freeProtocolHandle;
    }

    public ServerInitiativeProtocol setFreeProtocolHandle(FreeProtocolHandle freeProtocolHandle) {
        this.freeProtocolHandle = freeProtocolHandle;
        return this;
    }

    /**
     *  设置超时时间
     * @param timeout
     */
    @Override
    public ServerInitiativeProtocol timeout(long timeout) {
        if(timeout < 0) {
            throw new ProtocolException("超时时间必须大于 0(ms)");
        }

        this.timeout = timeout;
        return this;
    }

    @Override
    public abstract ProtocolType protocolType();

    /**
     * 是否进行同步请求处理,默认否
     * @return
     */
    @Override
    public boolean isSyncRequest(){
        return getDownLatch() != null;
    }

    @Override
    public void releaseLock() {
        if(isSyncRequest()) {
            getDownLatch().countDown();
        }
    }

    protected CountDownLatch getDownLatch() {
        return downLatch;
    }

    protected void setDownLatch(CountDownLatch downLatch) {
        this.downLatch = downLatch;
    }
}
